import React from 'react';

function Scores() {
  const localDB = JSON.parse(localStorage.db || []);
  const scores = localDB.map(({user, scores}) => ({
    user,
    'bestScore': scores
    .sort((first, second) => {
      return Number(first) < Number(second);
  })
    .reduce(prev => Number(prev))
  }))
  .sort((first, second) => {
    return Number(first.bestScore) < Number(second.bestScore);
  });

  return (
    <section className='col musix-scores text-center'>
      <div className='scores-card'>
        <h1 className="line-compress">Highest Scores</h1>
        <div className="clearfix"></div>
        <ul>
          {scores.map((score, index) => {
            return (
              <li key={index}>
                {score.user}
                <span className="d-block score-number">{score.bestScore}</span>
              </li>
              );
          })}
        </ul>
      </div>
    </section>
  );
}

export { Scores };