import React from 'react';
import _ from 'underscore';

export default function UserProfile(props) {
  console.log(props);
  const scores = [];
  const localDB = JSON.parse(localStorage.db) || false;

  if (localDB && localDB.length > 0) {
    localDB.forEach(key => {
      if (key.user === localStorage.username &&
        key.scores.length > 0) {
          key.scores.forEach(score => {
            scores.push(Number(score));
          });
        }
    });
  }

  scores.sort((a, b) => {
    return b - a;
  });

  function retrieveSessionUsername() {
    return localStorage.username || false;
  }

  function logout() {
    localStorage.removeItem('username');
    localStorage.removeItem('LATEST_QUIZ_SCORE');
    props.history.push('/');
  }

  function goToQuizPage() {
    props.history.push('/');
  }

  return (<section className="col text-center musix-user">
    <div className='scores-card'>
      <h1 className="line-compress">
        {retrieveSessionUsername() || 'Hey! take a quiz to save your profile'}
      </h1>
      <div className="clearfix"></div>
      <ul>
        {scores.map((score, index) => {
          return (
            <li key={index}>{score}</li>
            );
        })}
      </ul>
      </div>
      { retrieveSessionUsername() && 
      <div className="line fixed bottom left">
          <button onClick={logout}>
            Logout
          </button>
      </div>
      }
      { retrieveSessionUsername() ? null :
      <div className="line fixed bottom left">
          <button onClick={goToQuizPage}>
            Take the quiz
          </button>
      </div>
      }
    </section>
  )
}