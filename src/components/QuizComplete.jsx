import React from 'react';

export default function QuizComplete(props) {

  function saveUser() {
    if (!localStorage.db) {
      localStorage.db = JSON.stringify([{
        'user': localStorage.username,
        'scores': [localStorage.LATEST_QUIZ_SCORE]
      }]);
    } else if(localStorage.db) {

      let localDB = JSON.parse(localStorage.db) || false;
      let isInDB = false;

      localDB.forEach((key, value) => {
        if (key.user === localStorage.username) {
          key.scores.push(localStorage.LATEST_QUIZ_SCORE);
          isInDB = true;
        }
      });
      if (!isInDB) {
        localDB.push({
          'user': localStorage.username,
          'scores': [localStorage.LATEST_QUIZ_SCORE]
        });
      }
      localStorage.db = JSON.stringify(localDB);
    }
  }

  function goToUserPage() {
    props.history.push('/user');
  }

  function getUsername() {
    return localStorage.username || false;
  }

  function setUsername(event) {
    let user = event.target.value;
    localStorage.username = user;
  }

  return(
    <section className="col w-auto d-flex align-items-center justify-content-center text-center musix-quiz-complete">
      <h1>
        <span>Your Score is</span>
        <div className="score">{localStorage.getItem('LATEST_QUIZ_SCORE')}</div>
      </h1>
      <div className="actions">
        {getUsername() ? null : <input onChange={setUsername} type="text" placeholder="Insert your name" />}
        <button className="line fixed bottom left" type="button"
          onFocus={saveUser}
          onClick={goToUserPage}>
          Save
        </button>
      </div>
    </section>
  )
}
