import React, { useState, useEffect } from 'react';
import _ from 'underscore';
import configs from '../../config.json';

const WS_URL = configs.WS_URL;
const ARTISTS_LIST = configs.artists;
const MAX_ANS = configs.QUIZ_MAX_ANSWERS;
const PONTS = configs.QUIZ_CORRECT_ANSWER_POINTS;
/*eslint-disable*/
export default function Quiz(props) {
  const [songState, setSong] = useState(false);
  const [randomArtists, setRandomArtists] = useState([]);
  const [addArtist, setArtist] = useState('');
  const [quizAnswerState, setQuizAnswers] = useState(0);
  const [quizPointsState, setQuizPoints] = useState(0);

  let quizMaxAnswers = Number(MAX_ANS);
  let quizCorrectPointsValue = Number(PONTS);

  useEffect(() => {
    initQuiz();
  }, []);

  function initQuiz(){
    const getSong = async artist => {
        let getArtist = await fetch(`${WS_URL}artist?name=${artist}`)
          , resp1 = await getArtist.json()
          , artistID = JSON.parse(resp1).message.body.artist_list[0].artist.artist_id;

        console.info('Artist ID is:', artistID);

        let getArtistAlbumID = await fetch(`${WS_URL}artist/albums/?artist_id=${artistID}`)
          , resp2 = await getArtistAlbumID.json()
          , albums = JSON.parse(resp2).message.body.album_list
          , randomAlbum = albums[Math.floor(Math.random() * albums.length)]
          , albumID = randomAlbum.album.album_id;

        console.info('Album ID is:', albumID);

        let getAlbumSongID = await fetch(`${WS_URL}artist/album/song?album_id=${albumID}`)
          , resp3 = await getAlbumSongID.json()
          , tracks = JSON.parse(resp3).message.body.track_list
          , randomTrack = tracks[Math.floor(Math.random() * tracks.length)]
          , trackID = randomTrack.track.track_id;

        console.info('Track ID is:', trackID);

        let getLyric = await fetch(`${WS_URL}artist/song/snippet?track_id=${trackID}`)
          , resp4 = await getLyric.json()
          , snippet = JSON.parse(resp4).message.body.snippet.snippet_body;

        if (!snippet ||
          snippet && snippet.length <= 0) {
          throw 'Snippet arrived but is empty :/ :O woooot?';
        }
        return snippet;
    };

    console.info('######################################################');
    const artists = _.sample(ARTISTS_LIST, 3);
    const artist = _.sample(artists, 1)[0];

    console.info('Random artists are', artists);
    setRandomArtists(artists);
    console.info('Correct Answer is ----->', artist);
    setArtist(artist)

    getSong(artist).then(song => {
      setSong(song);
      console.info('Song Snippet Lyric is ---->', song);
    }).catch(err => {
      initQuiz();
      console.warn(err);
    });
  }

  function confirmAnswer(art) {
    setSong(false)
    setQuizAnswers(quizAnswerState + 1);
    if (art.toLowerCase() === addArtist.toLowerCase()) {
      setQuizPoints(quizPointsState + quizCorrectPointsValue);
    }
    if (quizAnswerState === quizMaxAnswers) {
      setQuizAnswers(0);
      localStorage.setItem('LATEST_QUIZ_SCORE', quizPointsState);
      console.log('Your score is ---->', quizPointsState, addArtist);
      props.history.push('/quizend');
    } else {
      initQuiz();
    }
  }

  return (
    <section className="col text-center musix-quiz">
      {!songState && 
        <div className="loader">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      }
        { songState && songState.length > 0 &&
          <h1 className="d-flex align-items-center justify-content-center animated fadeInDown shadow song">
            {songState}...
          </h1>
        }
        <div className="col d-flex fixed bottom left musix-answers">
        { songState && songState.length > 0 &&
        <div className="col p-0 d-flex flex-column animated fadeInUp">
          {randomArtists.map((artist, index) => {
            return (
              <a className={`col answer-${index}`} key={artist}
                onClick={() => {
                  confirmAnswer(artist);
                }}>
              {artist}
            </a>
            );
          })}
          </div>
        }
        </div>
    </section>
  )
}
/*eslint-enable*/