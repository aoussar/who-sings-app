import React from 'react';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlayCircle, faBookmark } from '@fortawesome/free-regular-svg-icons';
import { faUserCircle, faQuestionCircle} from '@fortawesome/free-solid-svg-icons';

export default function Navbar() {
  return (
    <section className="col d-flex text-center musix-menu">
      <NavLink exact to="/scores" activeClassName="active" className="col-3 d-flex flex-column text-center">
        <FontAwesomeIcon icon={faBookmark} />
        <span>Top Score</span>
      </NavLink>

      <NavLink exact to="/" activeClassName="active" className="col-4 d-flex flex-column text-center">
        <FontAwesomeIcon icon={faPlayCircle} />
        <span>Play Quiz</span>
      </NavLink>

      <NavLink exact to="/user" activeClassName="active" className="col-3 d-flex flex-column text-center">
        <FontAwesomeIcon icon={faUserCircle} />
        <span>My Profile</span>
      </NavLink>
    </section>
  );
}