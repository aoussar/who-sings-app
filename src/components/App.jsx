import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'csshelper/dist/helper.min.css';
import '../assets/scss/index.scss';
import 'animate.css/animate.min.css';
import Navbar from './Navbar.jsx';
import Quiz from './Quiz.jsx';
import QuizComplete from './QuizComplete.jsx';
import UserProfile from './UserProfile.jsx';
import { Scores } from './Scores.jsx';

if (!localStorage.getItem('db')) {
  localStorage.setItem('db', JSON.stringify([]));
}

export default function App() {
  return (
    <BrowserRouter>
      <div className="d-flex flex-column h-100">
        <Navbar></Navbar>
        <Route path="/user" exact component={UserProfile}/>
        <Route path="/" exact component={Quiz}/>
        <Route path="/quizend" exact component={QuizComplete}/>
        <Route path="/scores" exact component={Scores}/>
      </div>
    </BrowserRouter>
  )
}